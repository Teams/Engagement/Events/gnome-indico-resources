# GNOME Indico Resources

This repo contains the custom assets used on our live events website, https://events.gnome.org.

From the documentation listed here:
https://docs.getindico.io/en/stable/config/settings/#customization

> It is possible to override specific templates and add CSS and JavaScript for advanced customizations. When using this, be advised that depending on the modifications you perform things may break after an Indico update. Make sure to test all your modifications whenever you update Indico!

> To include custom CSS and JavaScript, simply put *.css and *.js files into <CUSTOMIZATION_DIR>/css / <CUSTOMIZATION_DIR>/js. If there are multiple files, they will be included in alphabetical order, so prefixing them with a number (e.g. 00-base.css, 10-events.css) is a good idea.

> Static files may be added in <CUSTOMIZATION_DIR>/files. They can be referenced in templates through the assets.custom endpoint. In CSS/JS, the URL for them needs to be built manually (/static/custom/files/...).

---

Indico cannot understand the .scss format, so CSS written in the .scss format must be converted to standard css.
The css/ directory within this repository should 'only' contain .css files, not .scss files.
Using .scss is purely optional, and is entirely for ease-of-development.

Note: The event CSS needs to be manually uploaded as custom-css in the Indico event "Customization>Layout" page. And, like above, any event CSS files need to be converted from .scss, which Indico cannot understand.

---

Javascript should be used sparingly, and is loaded automatically by Indico in alphanumerical order. Javascript is located in the 'js' directory and requires no special building with Node.JS