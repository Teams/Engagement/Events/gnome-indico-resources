jQuery(() => {
	// Add GUADEC Header if it is GUADEC
	if (jQuery('.conference-title-link').text().indexOf('GUADEC 2021') > -1) {
		const guadecDate = window.guadecDate ? window.guadecDate : 'July 21<sup>st</sup> – 25<sup>th</sup>, 2021';

		jQuery('.confheader').html('<div class="container">\n' +
			'        <div class="row hero-home">\n' +
			'            <div class="col">\n' +
			'                <div class="h1">\n' +
			'                   <div class="logo">\n' +
			'                    The\n' +
			'                    <img src="/static/custom/files/images/gnome-logo.svg" alt="GNOME">\n' +
			'                    Conference\n' +
			'\n' +
			'                   </div>\n' +
			'                </div>\n' +
			'                <div class="h1 guadec">\n' +
			'                    GUADEC\n' +
			'\n' +
			'                </div>\n' +
			'                <div class="h5">\n' +
			'                    ' + guadecDate + '\n' +
			'\n' +
			'\n' +
			'                </div>\n' +
			'            </div>\n' +
			'        </div>\n' +
			'    </div>');
	}
});
